# Slack cleaner

This is a python script to delete old files on Slack.
The code was adapted from https://github.com/PalmBeachPost/SlackPruner

Please, read the securuty notes in [creds_example.py](creds_example.py).

## Procedure
1.  Gather user API tokens from [https://api.slack.com/legacy/custom-integrations/legacy-tokens](https://api.slack.com/legacy/custom-integrations/legacy-tokens). **Warning! As per Slack policy, new tokens can be generated only until May 5 2020.** I have tokens for the `looselab` Slack, so we will be able to delete files from the current users also after this date. For the users that will join after May 5, the files won't be deleted in this way.
2.  Edit the creds_example.py file with user credentials and other settings. Simply substitute 
    `setup = {"domain": "looselab", "daystoretain": 30,  "apikeys":  [
("Chuck Norris", "xoxp-1234567890-1234567890-1234567890-1234567890"),
("Daenerys Targaryen", "xoxp-0123456789-0123456789-0123456789-0123456789")
    ]}`
with the selected slack domain, age the files have to be cleared and user data.
3. Run the  [Slack_cleaner_example]( Slack_cleaner_example.ipynb) as a Jupyter notbook or python script. 